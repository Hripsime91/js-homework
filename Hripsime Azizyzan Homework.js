// Part 1: Variables and Data Types
// a. Declare three variables using the var, let, and const keywords, respectively. Assign a string, a number, and a boolean value to each variable.

var anun = "Hripsime";
let name = "Shushanik";
const hasce = "Rubinyants";
console.log(hasce);
hasce = "Nalbandyan";
console.log(anun);

var tiv = 32;
let tariq = 25;
const tsnund = 1991;
console.log(tariq);

let compare = 5<15;
console.log(compare);
let compare_2 =5>15;
console.log(compare_2);
let compare_3 = true;
console.log(compare_3);


// b. Create a variable called myArray and assign an array of five numbers to it
let myArray = [25, 35, 45, 55, 65];
console.log(myArray);

// c. Use the typeof operator to print the data type of each variable to the console.
console.log(typeof myArray);
console.log(typeof(myArray));
console.log(typeof anun);
console.log(typeof(tariq));
console.log(typeof(compare));


// Part 2: Operators and Control Flow
// a. Create a function called isEven that takes a number as an argument and returns true if the number is even and false if it is odd.
function isEven(number) {
    if (number % 2 === 0) 
    return true
    else 
    return false

};
console.log(isEven(number=5));
console.log(isEven(4));

// b. Create a loop that prints the numbers from 1 to 20 to the console. For each number, print the number and whether it is even or odd (use the isEven function from the previous step).
const list_1 = [1, 2, 3, 4, 5, 6, 7, 8, 9,10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
for (n=0; n<list_1.length; n++) {
    if (isEven(n) % 2 === 0) 
    {console.log(list_1[n] + " is Even")}
    else {console.log(list_1[n] + " is Odd")}
};


// c. Create a function called calculate that takes two numbers and a string representing an arithmetic operator (+, -, *, or /) as arguments. The function should perform the arithmetic operation on the two numbers and return the result.
function calculator (n_1, n_2, i) {let output =0
    try {
        switch(i) {
            case "+":
                output= n_1 + n_2
                break;
            case "-":
                output = n_1-n_2
                break;
            case "*":
                output=n_1*n_2
                break;
            case "/":
                if (n_2===0) {
                    throw "Can't divide by 0!"
            
                }
                else {
                    output=n_1/n_2
                }
                break;
        }
    }
    catch (e) {
        console.log("There's an error: ", e)
    }
    return output;
}
console.log(calculator(2,5,"+"))

// Variant2
function calculator_1 (num_1, num_2, operand) {
    if (operand == "+") {console.log(num_1 + num_2)};
    if (operand == "-") {console.log(num_1 - num_2)};
    if (operand == "*") {console.log(num_1 * num_2)};
    if (operand == "/" && num_2 === 0) {console.log("Can't divide by 0")}
    else {(console.log(num_1 / num_2))};
};
console.log(calculator_1(2,0,"/"))

// Part 3: Functions and Objects
// a. Create an object called person with properties for name, age, and gender.
let person = {
    name: "Hripsime",
    age: 32,
    gender: "Female"
};

// b. Create a constructor function called Person that takes arguments for name, age, and gender, and sets the corresponding properties of a new Person object.
function Person(name, age, gender) {
    this.name=name;
    this.age=age;
    this.gender=gender
};
let person_2=new Person("Anna", "33", "female");
console.log(person_2)

// c. Create an array called people and add two Person objects to it.
let people = []
people.push(person, person_2)
console.log(person_2.name)
console.log(person.name)

// Part 4: JSON
// a. Convert the people array to a JSON string and store it in a variable called peopleJSON.
let peopleJSON = JSON.stringify(people)
console.log(peopleJSON)
// b. Parse the peopleJSON string back into an array called peopleFromJSON.
let peopleFromJSON = JSON.parse(peopleJSON)
console.log(peopleFromJSON)

// Part 5: Extra
// a. Create a function called mergeArrays that takes two arrays as arguments and returns a new array that contains all the elements from both arrays.
let arr_1 = [1, 2, 3]
let arr_2 = [4, 5, 6]
function mergeArrays(arr_1, arr_2) {
    let merge = [].concat(arr_1, arr_2)
    return merge
};
console.log(mergeArrays(arr_1, arr_2))
let new_1 = ["John", "Emil"]
let new_2 = ["Vachagan", "Parandzem"]
console.log(mergeArrays(new_1, new_2))

// b. Create a function called countOccurrences that takes an array of numbers and a number as arguments and returns the number of times the given number appears in the array
function countOccurrences(arr, value) {
    return arr.filter(a => (a === value)).length;
  };
console.log(countOccurrences([1,2,1,3,4], 1))


// Part 6: Promises
// a. Create a function called fetchData that uses the fetch function to retrieve data from an API. The function should return a Promise that resolves to the JSON data from the API.
// b. Use the fetchData function to retrieve data from an API and log the result to the console.

let status = fetch("https://apidev.onepick.me/login", {
       method: "POST",
       body: JSON.stringify({email:"armNn@mail.ru",password:"lilit123"}),
       headers: {
          "Content-Type": "application/json",
       }
    }
)
.then((response) => response.json())
.then(e=> console.log(e.status))